package com.efteaz.natitinterview.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.efteaz.natitinterview.R
import com.efteaz.natitinterview.service.model.CompetitionModel
import com.efteaz.natitinterview.service.repository.APIRepository
import com.efteaz.natitinterview.service.repository.APIResponse
import java.lang.Exception

/**
 * Created by Md Efteaz on 2019-11-06.
 */
class CompetitionViewModel(application: Application) : AndroidViewModel(application) {
    val context = application.applicationContext
    fun competition(token: String): LiveData<APIResponse<ArrayList<CompetitionModel>>>? {
        val mutableLiveData: MutableLiveData<APIResponse<ArrayList<CompetitionModel>>>? = MutableLiveData()

        if(token == null || token.isEmpty()) {
            mutableLiveData?.value = APIResponse<ArrayList<CompetitionModel>>().error(Exception(context.getString(R.string.error_empty_token)))
        } else {
            return APIRepository.competition(token)
        }

        return mutableLiveData
    }
}