package com.efteaz.natitinterview.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.efteaz.natitinterview.R
import com.efteaz.natitinterview.service.model.CompetitionModel
import kotlinx.android.synthetic.main.item_competition.view.*

/**
 * Created by Md Efteaz on 2019-11-06.
 */
class CompetitionRecyclerAdapter(competitionList: ArrayList<CompetitionModel>) :
    RecyclerView.Adapter<CompetitionRecyclerAdapter.ViewHolder>() {
    val lobbyList = competitionList

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        var view = LayoutInflater.from(parent.context).inflate(
            R.layout.item_competition, parent, false
        )
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return lobbyList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(lobbyList[position])
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(competitionModel: CompetitionModel) {
            with(itemView) {
                val name = competitionModel.name
                val category = competitionModel.category
                val prizeAmount = competitionModel.prizeAmount
                val prizeType = competitionModel.prizeType

                if (name != "null") {
                    tvName.text = name
                }

                if (category != "null") {
                    tvCategory.text = category
                }

                if (prizeType != "null") {
                    tvPrize.text = "$prizeAmount $prizeType"
                }

                //setting countries
                rvCountryFlag.layoutManager =
                    LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
                val countryFlagRecyclerAdapter =
                    CountryFlagRecyclerAdapter(competitionModel.countryFlagList)
                rvCountryFlag.adapter = countryFlagRecyclerAdapter
            }
        }
    }
}