package com.efteaz.natitinterview.view.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Html
import android.view.View
import android.view.animation.AnimationUtils
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.efteaz.natitinterview.R
import com.efteaz.natitinterview.service.model.CompetitionModel
import com.efteaz.natitinterview.service.repository.APIResponse
import com.efteaz.natitinterview.view.adapter.CompetitionRecyclerAdapter
import com.efteaz.natitinterview.viewmodel.CompetitionViewModel
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_competition.*
import kotlinx.android.synthetic.main.toolbar.*

class CompetitionActivity : AppCompatActivity() {

    //variables used in recyclerview it could be declared later too but it would be better for pagination
    private lateinit var competitionList: ArrayList<CompetitionModel>
    private lateinit var layoutManager: LinearLayoutManager
    private lateinit var competitionRecyclerAdapter: CompetitionRecyclerAdapter

    //view model
    private lateinit var viewModel: CompetitionViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_competition)

        viewModel = ViewModelProviders.of(this).get(CompetitionViewModel::class.java)

        initViews()

        //method to fetch dictionary from server
        competition("Token 141ab071fb53940394de287f7bb878dd82a95af3")

    }

    private fun initViews() {
        //setting title of page
        tvToolbarTitle.text = getString(R.string.competition_title)

        //configuring recyclerview with arraylist
        competitionList = ArrayList()
        layoutManager = LinearLayoutManager(this)
        rvCompetition.layoutManager = layoutManager
        competitionRecyclerAdapter = CompetitionRecyclerAdapter(competitionList)
        rvCompetition.adapter = competitionRecyclerAdapter


    }

    //api calls
    private fun competition(token: String) {
        //observing viewmodel and fetching data from server
        viewModel.competition(token)?.observe(this, Observer {
            when(it.status) {
                APIResponse.LOADING -> {
                    clLoader.visibility = View.VISIBLE
                }
                APIResponse.ERROR -> {
                    val loaderAnimation = AnimationUtils.loadAnimation(this, android.R.anim.fade_out)
                    loaderAnimation.fillAfter = true
                    clLoader.startAnimation(loaderAnimation)
                    clLoader.visibility = View.GONE

                    val snackbar = Snackbar.make(clLayout,  Html.fromHtml(it.error?.message), Snackbar.LENGTH_LONG)
                    val sbView = snackbar.view
                    sbView.setBackgroundColor(ContextCompat.getColor(this, R.color.colorError))
                    snackbar.show()
                }
                APIResponse.SUCCESS -> {
                    val loaderAnimation = AnimationUtils.loadAnimation(this, android.R.anim.fade_out)
                    loaderAnimation.fillAfter = true
                    clLoader.startAnimation(loaderAnimation)
                    clLoader.visibility = View.GONE

                    //adding competition items to recyclerview
//                    competitionList.clear() //if first page

                    competitionList.addAll(it.data!!)

                    if(competitionList.size > 0) {
                        competitionRecyclerAdapter.notifyDataSetChanged()
                    } else {
                        //showing message if empty list
                        val snackbar = Snackbar.make(clLayout,  getString(R.string.error_empty_competition), Snackbar.LENGTH_LONG)
                        val sbView = snackbar.view
                        sbView.setBackgroundColor(ContextCompat.getColor(this, R.color.colorError))
                        snackbar.show()
                    }
                }
            }
        })
    }

}
