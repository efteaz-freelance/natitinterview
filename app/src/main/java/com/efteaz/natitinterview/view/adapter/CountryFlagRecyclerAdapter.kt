package com.efteaz.natitinterview.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.efteaz.natitinterview.R
import kotlinx.android.synthetic.main.item_country_flag.view.*

/**
 * Created by Md Efteaz on 2019-11-07.
 */
class CountryFlagRecyclerAdapter(countryFlagList: ArrayList<String>) : RecyclerView.Adapter<CountryFlagRecyclerAdapter.ViewHolder>() {
    val countryFlagList = countryFlagList

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        var view = LayoutInflater.from(parent.context).inflate(
            R.layout.item_country_flag, parent, false
        )
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return countryFlagList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(countryFlagList[position])
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(countryFlagUrl: String) {
            with(itemView) {
                if (countryFlagUrl != "null" && countryFlagUrl.isNotEmpty()) {
                    Glide.with(this)
                        .load(countryFlagUrl)
                        .apply(
                            RequestOptions()
                                .diskCacheStrategy(DiskCacheStrategy.ALL)
                        )
                        .into(ivCountryFlag)
                }
            }
        }
    }
}