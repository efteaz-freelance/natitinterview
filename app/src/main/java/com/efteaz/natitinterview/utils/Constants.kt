package com.efteaz.natitinterview.utils

/**
 * Created by Md Efteaz on 2019-11-06.
 */
object Constants {
    //base url of api
    const val BASE_URL = "http://111.93.169.90:8002/"

    //api endpoints
    const val API_COMPETITION = "competition/lobby/"

    //api parameters
    const val PR_AUTHORIZATION = "Authorization"
}