package com.efteaz.natitinterview.service.repository

import com.efteaz.natitinterview.utils.Constants
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Headers

/**
 * Created by Md Efteaz on 2019-11-06.
 */
interface APIInterface {
    @Headers("Content-Type: application/json")
    @GET(Constants.API_COMPETITION)
    fun competition(
        @Header(Constants.PR_AUTHORIZATION) token: String
    ): Call<ResponseBody>
}