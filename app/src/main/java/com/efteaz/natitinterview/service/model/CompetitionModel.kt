package com.efteaz.natitinterview.service.model

/**
 * Created by Md Efteaz on 2019-11-06.
 */
data class CompetitionModel(val id: Int, val name: String, val category: String, val prizeAmount: Int, val prizeType: String, val countryFlagList: ArrayList<String>) {
}