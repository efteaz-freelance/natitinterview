package com.efteaz.natitinterview.service.repository

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.efteaz.natitinterview.service.model.CompetitionModel
import com.efteaz.natitinterview.utils.Constants
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by Md Efteaz on 2019-11-06.
 */
object APIRepository {
    val apiInterface = APIClient.retrofit(Constants.BASE_URL)

    //server call to fetch dictionary
    fun competition(token: String): LiveData<APIResponse<ArrayList<CompetitionModel>>> {
        val mutableLiveData = MutableLiveData<APIResponse<ArrayList<CompetitionModel>>>()

        mutableLiveData.value = APIResponse<ArrayList<CompetitionModel>>().loading()

        val call = apiInterface.competition(token)
        call.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {

                        val resultArray =
                            JSONObject(response.body()?.string()).optJSONArray("results")

                        val arrayList = ArrayList<CompetitionModel>()
                        resultArray.let {
                            for (i in 0 until it.length()) {
                                val resultObject = resultArray.optJSONObject(i)
                                val id = resultObject.optInt("id")
                                val name = resultObject.optString("name")
                                val category = resultObject.optString("category")
                                val prizeAmount = resultObject.optInt("prizeAmount")
                                val prizeType = resultObject.optString("prizeType")

                                val countryFlagList = ArrayList<String>()

                                val countryFlagArray = resultObject.optJSONArray("countryFlagList")
                                countryFlagArray.let {
                                    for (position in 0 until it.length()) {
                                        countryFlagList.add(countryFlagArray.optString(position))
                                    }
                                }

                                val model = CompetitionModel(id, name, category, prizeAmount, prizeType, countryFlagList)
                                arrayList.add(model)

                            }
                        }
                        mutableLiveData.value =
                            APIResponse<ArrayList<CompetitionModel>>().success(arrayList)
                    } catch (e: Exception) {
                        e.printStackTrace()
                        mutableLiveData.value =
                            APIResponse<ArrayList<CompetitionModel>>().error(Exception(e.message.toString()))
                    }

                } else {
                    mutableLiveData.value =
                        APIResponse<ArrayList<CompetitionModel>>().error(Exception(response.errorBody()?.string()))
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                t.printStackTrace()
                if(t.message.toString().contains("Failed to connect")) {
                    mutableLiveData.value =
                        APIResponse<ArrayList<CompetitionModel>>().error(Exception("Please check your network connection"))
                } else {
                    mutableLiveData.value =
                        APIResponse<ArrayList<CompetitionModel>>().error(Exception(t.message.toString()))
                }
            }
        })

        return mutableLiveData
    }
}